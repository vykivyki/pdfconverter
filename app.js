﻿'use strict'

var http = require('http'),
	express = require('express'),
    bodyParser = require('body-parser'),
	PDFDocument = require('pdfkit'),
	fs = require('fs');

var urlencodedParser = bodyParser.urlencoded({ extended:false });
	
var app = express();

app.set('views', './views');
app.set('view engine', 'jade');

app.use(bodyParser.urlencoded({ extended:false }));
app.use(bodyParser.json());



app.get('/', function(req, res){
	res.render('index');
});

app.post('/', urlencodedParser, function (req, res) {
    if (!req.body) return res.sendStatus(400)
    var bodyData = req.body

    makepdf(PDFDocument,bodyData)

	setTimeout(function() {res.download(__dirname +'/out.pdf')}, 500);
});

function makepdf(PDFDocument, bodyData){
    var doc = new PDFDocument();
    var stream = doc.pipe(fs.createWriteStream(__dirname + '/out.pdf'));
	
	doc.image(__dirname + '/form.jpg', 0, 0, {scale :0.25})
    doc.font(__dirname + '/fonts/FreeSans.ttf',12)
	
    doc.text('«'+bodyData.textEvent+'»', 160, 289,{width: 450})
    .moveDown()
	doc.text(bodyData.textDate, 160, 317,{width: 450})
    .moveDown()
	doc.text('«'+bodyData.textCompanyName+'»', 160, 377,{width: 300, align: 'center'})
	.moveDown()
	doc.text(bodyData.textUserFirstName, 160, 479,{width: 450})
	.moveDown()
	doc.text(bodyData.textUserLastName, 160, 504,{width: 450})
	.moveDown()
	doc.text(bodyData.textAddress, 165, 606,{width: 450})
	.moveDown()
	doc.text(bodyData.textEmail, 165, 634,{width: 450, link: 'mailto:'+bodyData.textEmail, underline: true, fillcolor: 'red'})
	.moveDown()
	doc.text(bodyData.textTel1+"; ", 165, 662,{width: 450})
	.moveDown()
	doc.text(bodyData.textTel2, doc.widthOfString(bodyData.textTel1+"; ")+165, 662,{width: 450})
    doc.end();
}

http.createServer(app).listen(8899, function () {
    console.log('http://localhost:8899')
});